import { useState, useEffect } from "react"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Router from "next/router"
import Head from "next/head"
import Swal from "sweetalert2"

export default function register() {

	//states for our input
	const [ email, setEmail ] = useState("")
	const [ password1, setPassword1 ] = useState("")
	const [ password2, setPassword2 ] = useState("")

	//validate form input
	const [ isActive, setIsActive ] = useState(false)

	//form validation which conditionally renders our submit button
	useEffect(() => {

		if((password1 !== "" && password2 !== "" && email !== "" ) && password1 === password2){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	}, [password1, password2])

	function register(e){

		e.preventDefault()

		//getch request to register our user. But first, let's check if the DB already has the same email
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/email-exists`, {

			method: "POST",
			headers: {

				"Content-Type": "application/json"

			},
			body: JSON.stringify({

				email: email

			})

		}).then( res => res.json())
		.then( data => {

			if(data === false){

				//proceed to registration
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users`, {
					method: "POST",
					headers: {

						"Content-Type": "application/json"

					},
					body: JSON.stringify({

						email: email,
						password: password1

					})
				}).then( res => res.json())
				.then( data => {
					if( data === true ){

						Swal.fire({

							icon: "success",
							title: "Registration Successful!"

						})
						Router.push("/login")

					} else {

						Swal.fire({

							icon: "error",
							title: "Something Went Wrong...",
							text: "The account has not been registered"

						})

					}

				})

			} else{

				Swal.fire({

					icon: "error",
					title: "That email has already been used."

				})
				setEmail("")
				setPassword1("")
				setPassword2("")

			}

		})

	}

	return(

			<>
				<Head>
					<title>Register || Budget Tracker</title>
				</Head>
				<h1>Register an Account</h1>
				<Form className="mt-3" onSubmit ={(e) => register(e)}>
					<Form.Group controlId="email">
					<Form.Label>Email Address:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="password2">
					<Form.Label>Confirm Password:</Form.Label>
						<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
					</Form.Group>
					{
						isActive ?
						<Button variant="primary" type="submit">Submit</Button> :
						<Button variant="primary" type="submit" disabled>Submit</Button> 
					}
				</Form>
			</>

		)
}
