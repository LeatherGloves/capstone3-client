import { useState, useEffect, useContext } from "react"
import { Pie } from "react-chartjs-2"
import { defaults } from "react-chartjs-2"
import { colorRandomizer } from "../helpers"
import { Row, Col } from "react-bootstrap"
import Form from "react-bootstrap/Form"
import moment from "moment"
import UserContext from "../UserContext"

export default function breakdown(){

	const [ categories, setCategories ] = useState([])
	const [ amount, setAmount ] = useState([])
	const [ bgColors, setBgColors ] = useState([])
	const [ dataSet, setDataSet ] = useState([])
	const [ beforeDate, setBeforeDate ] = useState("2020-12-31")
	const [ afterDate, setAfterDate ] = useState("2020-01-01")
	const { user } = useContext(UserContext)

	defaults.global.defaultFontColor = 'White'

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/`,{
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then( res => res.json())
		.then( data => {
			setDataSet(data)

		})

	}, [])

	useEffect(() => {

		let categoryNames = []
		dataSet.forEach( element => {

			if(!categoryNames.find(make => make === element.categoryName) && element.createdBy === user.id){
				let currentMoment = moment(element.createdOn).format("YYYY-MM-DD")
				if((moment(currentMoment).isBefore(beforeDate)) && (moment(currentMoment).isAfter(afterDate))){
					categoryNames.push(element.categoryName)
				}
			}

		})

		setCategories(categoryNames)

	}, [dataSet])

	useEffect(() => {
		setAmount(categories.map(category => {

			let amounts = 0

			dataSet.forEach( element => {

				if(element.categoryName === category && element.amount !== undefined){
					let currentMoment = moment(element.createdOn).format("YYYY-MM-DD")
					if((moment(currentMoment).isBefore(beforeDate)) && (moment(currentMoment).isAfter(afterDate))){
						amounts = amounts + element.amount
					}


				}

			})

			return amounts
		}))



	}, [categories, beforeDate, afterDate])

	useEffect(() => {
		setBgColors(categories.map(() => `#${colorRandomizer()}`))
	}, [categories])

	const data = {

		labels: categories,
		datasets : [
			{

				data: amount,
				backgroundColor: bgColors,
				hoverBackgroundColors: []

			}
		]

	}

	return(
			<>
				<Row>
					<Col>
						<Form.Control type="date" value={afterDate} onChange={e => setAfterDate(e.target.value)} required/>
					</Col>
					<Col>
						<Form.Control type="date" value={beforeDate} onChange={e => setBeforeDate(e.target.value)} required/>
					</Col>
				</Row>	
				<Pie data={data} />
			</>
		)

}