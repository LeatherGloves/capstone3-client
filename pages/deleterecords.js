import { useState, useEffect, useContext } from "react"
import { useRouter } from "next/router" 
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Router from "next/router"
import Head from "next/head"
import Swal from "sweetalert2"
import UserContext from "../UserContext"

export default function deleterecords(){

	const router = useRouter()
	const recordId = router.query.recordId
	const { user } = useContext(UserContext)
	const userId = user.id

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/delete/${recordId}`, {
			method : "DELETE",
			headers : {
				"Authorization" : `Bearer ${localStorage.token}`
			}
		})
		.then (data => {
			console.log(data)
		})
	})

	return(

			<>
				<Head>
					<title>Delete a Record || Budget Tracker</title>
				</Head>
				<h1>Deletion Time</h1>
			</>

		)
}