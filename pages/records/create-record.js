import { useState, useEffect, useContext } from "react"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Router from "next/router"
import Head from "next/head"
import Swal from "sweetalert2"
import UserContext from "../../UserContext"

export default function newEntry(){

	const [ name, setName ] = useState("")
	const [ type, setType ] = useState("Expense")
	const [ description, setDescription ] = useState("")
	const [ amount, setAmount ] = useState(-0)
/*	const [ dateCreated, setDateCreated ] = useState(new Date())*/
	const [ isActive, setIsActive ] = useState(false)
	const [ categories, setCategories] = useState([])

	const { user } = useContext(UserContext)
	const userId = user.id

	useEffect(() => {

		if(name !== ""&& name !== "Choose Category" && description !== "" && amount !== 0 && amount !== ""){
			if((type === "Expense" && amount < 0) || (type ==="Income" && amount > 0)){
				setIsActive(true)				
			} else setIsActive(false)
		} else {
			setIsActive(false)
		}

	}, [name, type, description, amount])

	useEffect(() => {

		if(type === "Expense"){
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/expenses`, {

			}).then( res => res.json())
			.then( data => {
				
				setCategories(data)
			})
		} else {
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/income`, {

			}).then( res => res.json())
			.then( data => {
				setCategories(data)

			})
		}
	}, [type])

	function createEntry(e) {

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/addEntry`, {
			method: "POST",
			headers: {

				"Content-Type": "application/json"

			},
			body: JSON.stringify({

				categoryType: type,
				categoryName: name,
				description: description,
				amount: amount,
				userId: userId

			})
		}).then( res => res.json())
		.then( data => {
			if( data === true ){

				Swal.fire({

					icon: "success",
					title: "Created a new entry!"

				})
				Router.push("/records")

			} else {

				Swal.fire({

					icon: "error",
					title: "Something Went Wrong...",
					text: "The entry hasn't been saved"

				})

			}

		})
	}
	const CategoriesList = categories.map(category => {
		if(category.createdBy === userId && category.description === undefined){

			return(

					<option key={category._id}>{category.categoryName}</option>

				)
		} else return null
	})

	return(

			<>
				<Head>
					<title>Create New Record || Budget Tracker</title>
				</Head>
				<Form onSubmit ={(e) => createEntry(e)}>
					<Form.Group controlId="categoryType">
					  <Form.Label>Type:</Form.Label>
					  <Form.Control as="select" value={type} onChange={e => setType(e.target.value)} custom>
					    <option>Expense</option>
					    <option>Income</option>
					  </Form.Control>
					</Form.Group>
					<Form.Group controlId="categoryName">
						<Form.Label>Category Name:</Form.Label>
						<Form.Control as="select" value={name} onChange={e => setName(e.target.value)} custom>
							<option>Choose Category</option>
							{CategoriesList}
						</Form.Control>
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label>Desription:</Form.Label>
						<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="amount">
						<Form.Label>Amount (If expense, input negative value):</Form.Label>
						<Form.Control type="number" placeholder="Enter Cost/Income" value={amount} onChange={e => setAmount(e.target.value)} required/>
					</Form.Group>
					{
						isActive ?
						<Button variant="primary" type="submit">Submit</Button> :
						<Button variant="primary" type="submit" disabled>Submit</Button> 
					}
				</Form>
			</>

		)
}

/*<Form.Group controlId="date">
	<Form.Label>Date Created:</Form.Label>
	<Form.Control type="date" placeholder={dateCreated} value={dateCreated} onChange={e => setDateCreated(e.target.value)} required/>
</Form.Group>*/