import { useState, useEffect, useContext } from "react"
import Router from "next/router"
import Head from "next/head"
import UserContext from "../../UserContext"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form"
import { Row } from "react-bootstrap"
import { Col } from "react-bootstrap"
import Table from "react-bootstrap/Table"
import { Fragment } from 'react' 
import InputGroup from "react-bootstrap/InputGroup"

export default function records(){

	const [ dataSet, setDataSet ] = useState([])
	const [ userDataSet, setUserDataSet ] = useState([])
	const [ type, setType ] = useState("Expense & Income")
	const [ categoryName, setCategoryName ] = useState("")
	const { user } = useContext(UserContext)
	let totalMoney = 0

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/`,{
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then( res => res.json())
		.then( data => {
			setDataSet(data)

		})

	}, [])
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`,{
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.token}`
			}
		})
		.then( res => res.json())
		.then( data => {
			setUserDataSet(data)

		})

	}, [])

	const RecordsSet = dataSet.map(record => {
		if(record.amount !== undefined && record.description !== undefined && record.createdBy === user.id){
			totalMoney += record.amount
			if(categoryName !== "") {
				if(record.description.search(categoryName) !== -1){
					return(
							<tr key={record._id}>
							<td>{record.categoryName} <inline className={record.categoryType}>({record.categoryType})</inline></td>
								<td>{record.description}</td>
								<td className={record.categoryType}>{record.amount}</td>
								<td className={record.categoryType}>{totalMoney}</td>
								<Button variant="success" href={`https://nameless-citadel-42107.herokuapp.com/ editrecords?recordId=${record._id}`}>Edit</Button>
								<Button variant="danger" href={`https://nameless-citadel-42107.herokuapp.com/ deleterecords?recordId=${record._id}`}>Delete</Button>
							</tr>
						)
				}
			} else if (type === "Expense & Income"){
					return(

						<tr key={record._id}>
						<td>{record.categoryName} <inline className={record.categoryType}>({record.categoryType})</inline></td>
							<td>{record.description}</td>
							<td className={record.categoryType}>{record.amount}</td>
							<td className={record.categoryType}>{totalMoney}</td>
							<Button variant="success" href={`https://nameless-citadel-42107.herokuapp.com/ editrecords?recordId=${record._id}`}>Edit</Button>
							<Button variant="danger" href={`https://nameless-citadel-42107.herokuapp.com/ deleterecords?recordId=${record._id}`}>Delete</Button>
						</tr>

				)			
			} else if (record.categoryType === type){
					return(

						<tr key={record._id} className="mt-3">
							<td>{record.categoryName} <inline className={type}>({record.categoryType})</inline></td>
							<td>{record.description}</td>
							<td className={type}>{record.amount}</td>
							<td className={type}>{totalMoney}</td>
							<Button variant="success" href={`https://nameless-citadel-42107.herokuapp.com/ editrecords?recordId=${record._id}`}>Edit</Button>
							<Button variant="danger" href={`https://nameless-citadel-42107.herokuapp.com/ deleterecords?recordId=${record._id}`}>Delete</Button>
						</tr>

				)
			} 
		}else return null
	})
	function createRecord(){
		Router.push("/records/create-record")
	}

	return(

		<Fragment>
			<Head>
				<title> Records || Budget Tracker</title>
			</Head>
			<Row>
				<Col xs={12}>
					<h1>Records</h1>
				</Col>
				<Col md={8}>
					<InputGroup size="md" className="mb-3">
						<InputGroup.Prepend>
							<Button variant="success" type="button" onClick={() => createRecord()}>Create New</Button>
						</InputGroup.Prepend>
					  <Form.Control aria-label="Small" placeholder="Search here"aria-describedby="inputGroup-sizing-sm" value={categoryName} onChange={e => setCategoryName(e.target.value)} />
					</InputGroup>
				</Col>
				<Col md={4}>	
					<Form.Control as="select" value={type} onChange={e => setType(e.target.value)} custom>
					  <option>Expense & Income</option>
					  <option>Expense</option>
					  <option>Income</option>
					</Form.Control>
				</Col>
				<Col xs={12}>
					<Table striped bordered hover variant="dark" responsive>
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Funds</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{RecordsSet}
						</tbody>
					</Table>
				</Col>
			</Row>
		</Fragment>

	)

}