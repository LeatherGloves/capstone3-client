import Banner from "../components/Banner"
import Navbar from "../components/Navbar"
import Head from "next/head"
export default function Home(){

  const data = {

    title: 'Budget Tracker',
    content: 'Manage your Budget with ease.'

  }
  return(
    
    <>
      <Head>
        <title>Home || Budget Tracker</title>
      </Head>
      <Banner data={data} />
    </>

    )

}