import "bootstrap/dist/css/bootstrap.min.css"
import '../styles/globals.css'
import { Container } from "react-bootstrap"
import Navbar from "../components/Navbar"
import { UserProvider } from "../UserContext"
import { useState, useEffect } from "react"

function MyApp({ Component, pageProps }) {

	const [ user, setUser ] = useState({

		id: null

	})

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {

			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}

		}).then( res => res.json())
		.then( data => {

			if(data._id){
				setUser({
					id: data._id
				})
			} else{
				setUser({
					id: null
				})
			}

		})
	}, [user.id])

	const unsetUser = () => {
		localStorage.clear()

		setUser({

			id: null

		})
	}

  return(
  		<>
  			<UserProvider value={{user, setUser, unsetUser}}>
	  			<Navbar />
		  		<Container>
		  			<Component {...pageProps} />
		  		</Container>
	  		</UserProvider>
	  	</>
  	) 

}

export default MyApp
