import { useState, useEffect, useContext } from "react"
import { Bar } from "react-chartjs-2"
import { defaults } from "react-chartjs-2"
import moment from "moment"
import UserContext from "../UserContext"

export default function monthlyExpense(){

	const [ dataSet, setDataSet ] = useState([])
	const [ months, setMonths ] = useState([])
	const [ monthlyExpense, setMonthlyExpense ] = useState(0)
	const monthsReference = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	const { user } = useContext(UserContext)

	defaults.global.defaultFontColor = 'White'
	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/expenses`, {

		}).then( res => res.json())
		.then( data => {
			
			setDataSet(data)
		})
	}, [])
	useEffect(() => {
		if (dataSet.length > 0){

			let tempMonths = []
			dataSet.forEach(element => {
				if(!tempMonths.find(month => month === moment(element.createdOn).format("MMMM"))&& element.amount !== undefined && element.createdBy === user.id){
					tempMonths.push(moment(element.createOn).format("MMMM"))
				}
			})


			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

			tempMonths.sort((a, b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexof(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}
			})
			setMonths(tempMonths)
		}
	}, [dataSet])

	useEffect(() => {

		setMonthlyExpense(monthsReference.map(month => {

			let expense = 0

			dataSet.forEach(element => {
				if(moment(element.createdOn).format("MMMM") === month && element.amount !== undefined && element.createdBy === user.id){

						expense = expense + element.amount

				}

			})

			return expense

		}))

	}, [months])

	const data = {

		labels: monthsReference,
		datasets: [
			{
				label: "Monthly Expenses for the Year",
				backgroundColor: "orangered",
				borderColor: "black",
				borderWidth: 1,
				hoverBackgroundColor: "tomato",
				hoverBorderColor: "black",
				data: monthlyExpense,
				fontColor: "white"


			}

		]

	}

	return(

			<Bar data={data} />

		)

}