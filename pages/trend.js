import { useState, useEffect, useContext } from "react"
import { Line } from "react-chartjs-2"
import { defaults } from "react-chartjs-2"
import { Row, Col } from "react-bootstrap"
import UserContext from "../UserContext"
import moment from "moment"
import Form from "react-bootstrap/Form"

export default function trend(){

	const [ idNumber, setIdNumber ] = useState([])
	const [ amount, setAmount ] = useState([])
	const [ bgColors, setBgColors ] = useState([])
	const [ dataSet, setDataSet ] = useState([])
	const [ beforeDate, setBeforeDate ] = useState("2020-12-31")
	const [ afterDate, setAfterDate ] = useState("2020-01-01")
	const { user } = useContext(UserContext)

	defaults.global.defaultFontColor = 'White'

	let amounts = 0

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/`,{
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then( res => res.json())
		.then( data => {
			setDataSet(data)

		})

	}, [])

	useEffect(() => {

		let categoryIds = []
		dataSet.forEach( element => {

			if(!categoryIds.find(make => make === element._id) && element.amount !== undefined && element.createdBy === user.id){
				let currentMoment = moment(element.createdOn).format("YYYY-MM-DD")
				if((moment(currentMoment).isBefore(beforeDate)) && (moment(currentMoment).isAfter(afterDate))){
					categoryIds.push(element._id)
				}
			}

		})

		setIdNumber(categoryIds)

	}, [dataSet, beforeDate, afterDate])

	useEffect(() => {

		setAmount(idNumber.map(idNumber => {


			dataSet.forEach( element => {
				if(element._id === idNumber && element.amount !== undefined && element.createdBy === user.id){
 					let currentMoment = moment(element.createdOn).format("YYYY-MM-DD")
 					if((moment(currentMoment).isBefore(beforeDate)) && (moment(currentMoment).isAfter(afterDate))){
						amounts = amounts + element.amount
					}
				}

			})
			return amounts
		}))

	}, [idNumber, beforeDate, afterDate])

	let bgColor;
	let bgBorderColor
	let totalAmount = amount[amount.length - 1]
	if(totalAmount < 0 ){
		bgBorderColor = "orangered"
		bgColor = "tomato"
	} else {
		bgBorderColor = "green"
		bgColor = "forestgreen"
	}

	const data = {

		labels: amount,
		datasets : [ 
			{
				label: "Money Trends",
				data: amount,
				backgroundColor: bgColor,
				borderColor: bgBorderColor,
				hoverBackgroundColors: []

			}
		]

	}

	return(
		<>
			<Form.Group controlId="dates">
				<Row>
					<Col>
						<Form.Control type="date" value={afterDate} onChange={e => setAfterDate(e.target.value)} required/>
					</Col>
					<Col>
						<Form.Control type="date" value={beforeDate} onChange={e => setBeforeDate(e.target.value)} required/>
					</Col>
				</Row>	
			</Form.Group>
			<Line data={data} />
		</>
		)

}	
