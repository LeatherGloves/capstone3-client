import { useState, useEffect, useContext } from "react"
import Router from "next/router"
import Head from "next/head"
import UserContext from "../UserContext"

import { GoogleLogin } from "react-google-login"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import { Fragment } from "react"
import Swal from "sweetalert2"

export default function login(){

	const { setUser } = useContext(UserContext)

	const [ email, setEmail] = useState("")
	const [ password, setPassword] = useState("")

	function authenticate(e) {

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/login`, {
			method: "POST",
			headers: {

				"Content-Type": "application/json"

			},
			body: JSON.stringify({
			
				email: email,
				password: password

			})
		}).then( res => res.json())
		.then( data => {

			if(data.accessToken){

				localStorage.setItem("token", data.accessToken)

				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}
				}).then( res => res.json())
				.then( data => {

					setUser({

						id: data._id

					})

					Router.push("/")

				})

			} else {

				Swal.fire({

					icon: "error",
					title: "Authentication Failed"

				})

			}

		})

		setEmail("")
		setPassword("")

	}

	const authenticateGoogleToken = (response) => {

		const payload = {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				googleToken: response.accessToken

			})
		}
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/verify-google-id-token`, payload)
		.then( res => res.json())
		.then( data => {
			if(data.accessToken){
				localStorage.setItem("token", data.accessToken)
				fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
					headers: {
						"Authorization": `Bearer ${data.accessToken}`
					}
				}).then( res => res.json())
				.then( data => {
					setUser({

						id: data._id,
						email: data.email

					})
				})
				
				Router.push("/")

			}else {

				if( data.error === "google-auth-error" ) {

					Swal.fire({

						icon: "error",
						title: "Google Authentication Error.",
						text: "Google Authenticaation Procedure has failed, try again or contact your web admin."

					})
				} else if (data.error === "login-type-error") {

					Swal.fire({

						icon: "error",
						title: "Login Type Error",
						text: "You may ahve registered using a different login procedure. Try alternative login procedures."

					})
				}
			}
		})
	}

	return(

			<Fragment>
				<Head>
					<title>Login || Budget Tracker</title>
				</Head>
				<Form className="mt-3" onSubmit={(e) => authenticate(e)}>
					<Form.Group controlId="emailAdd">
						<Form.Label>Email Address:</Form.Label>
						<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e) => setEmail(e.target.value)} required />
					</Form.Group>
					<Form.Group controlId="password">
						<Form.Label>Password:</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Button variant="primary" type="submit" className="mb-3"block>Submit</Button>
					<GoogleLogin

						clientId="8790698012-crvuu1ldmcfv0qk9gf06rj9hiasuh0vt.apps.googleusercontent.com"
						buttonText="Login using Google"
						cookiepolicy={"single_host_origin"}
						className="w-100 text-center d-flex justify-content-center"
						onSuccess={ authenticateGoogleToken }
						onFailure={ authenticateGoogleToken }
					/>
				</Form>
			</Fragment>

		)

}