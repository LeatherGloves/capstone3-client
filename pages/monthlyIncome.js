import { useState, useEffect, useContext } from "react"
import { Bar } from "react-chartjs-2"
import moment from "moment"
import { defaults } from "react-chartjs-2"
import UserContext from "../UserContext"

export default function monthlyIncome(){

	const [ dataSet, setDataSet ] = useState([])
	const [ months, setMonths ] = useState([])
	const [ monthlyIncome, setMonthlyIncome ] = useState(0)
	const monthsReference = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	const { user } = useContext(UserContext) 

	defaults.global.defaultFontColor = 'White'

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/income`, {

		}).then( res => res.json())
		.then( data => {
			setDataSet(data)
		})

		}, [])
	useEffect(() => {
		if (dataSet.length > 0){

			let tempMonths = []
			dataSet.forEach(element => {
				if(!tempMonths.find(month => month === moment(element.createdOn).format("MMMM")) && element.amount !== undefined && element.createdBy === user.id){
					tempMonths.push(moment(element.createOn).format("MMMM"))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

			tempMonths.sort((a, b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexof(b) !== -1){

					return monthsRef.indexOf(a) - monthsRef.indexOf(b)

				}
			})
			setMonths(tempMonths)
		}
	}, [dataSet])

	useEffect(() => {

		setMonthlyIncome(monthsReference.map(month => {

			let income = 0

			dataSet.forEach(element => {
				if(moment(element.createdOn).format("MMMM") === month && element.amount !== undefined && element.createdBy === user.id){
					income = income + element.amount
				}

			})

			return income

		}))

	}, [months])

	const data = {

		labels: monthsReference,
		datasets: [
			{
				label: "Monthly Income for the Year",
				backgroundColor: "lawngreen",
				borderColor: "black",
				borderWidth: 1,
				hoverBackgroundColor: "greenyellow",
				hoverBorderColor: "black",
				data: monthlyIncome


			}

		]

	}

	return(

			<Bar data={data} />

		)

}