import { useState, useEffect, useContext } from "react"
import Form from "react-bootstrap/Form"
import Button from "react-bootstrap/Button"
import Router from "next/router"
import Head from "next/head"
import Swal from "sweetalert2"
import UserContext from "../../UserContext"

export default function newCategory(){

	const [ name, setName ] = useState("")
	const [ type, setType ] = useState("Expense")
	const [ isActive, setIsActive ] = useState(false)

	const { user } = useContext(UserContext)
	const userId = user.id

	useEffect(() => {

		if(name !== ""){
			setIsActive(true)				
		} else {
			setIsActive(false)
		}

	}, [name, type])


	function createCategory(e) {

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/addCategory`, {
			method: "POST",
			headers: {

				"Content-Type": "application/json"

			},
			body: JSON.stringify({

				categoryType: type,
				categoryName: name,
				userId: userId

			})
		}).then( res => res.json())
		.then( data => {
			if( data === true ){

				Swal.fire({

					icon: "success",
					title: "Created a new Category!"

				})
				Router.push("/categories")

			} else {

				Swal.fire({

					icon: "error",
					title: "Something Went Wrong...",
					text: "The entry hasn't been saved"

				})

			}

		})
	}

	return(

			<>
				<Head>
					<title>Create New Category || Budget Tracker</title>
				</Head>
				<Form onSubmit ={(e) => createCategory(e)}>
					<Form.Group controlId="categoryType">
					  <Form.Label>Type:</Form.Label>
					  <Form.Control as="select" value={type} onChange={e => setType(e.target.value)} custom>
					    <option>Expense</option>
					    <option>Income</option>
					  </Form.Control>
					</Form.Group>
					<Form.Group controlId="categoryName">
						<Form.Label>Category Name:</Form.Label>
						<Form.Control type="text" placeholder="Enter Category Name" value={name} onChange={e => setName(e.target.value)} required/>
					</Form.Group>
					{
						isActive ?
						<Button variant="primary" type="submit">Submit</Button> :
						<Button variant="primary" type="submit" disabled>Submit</Button> 
					}
				</Form>
			</>

		)
}