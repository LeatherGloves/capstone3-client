import { useState, useEffect, useContext } from "react"
import Router from "next/router"
import Head from "next/head"
import UserContext from "../../UserContext"
import Button from "react-bootstrap/Button"
import Card from "react-bootstrap/Card"
import { Row } from "react-bootstrap"
import { Col } from "react-bootstrap"
import Table from "react-bootstrap/Table"
import { Fragment } from 'react' 

export default function categories(){

	const [ dataSet, setDataSet ] = useState([])
	const { user } = useContext(UserContext)

	useEffect(() => {
		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/entries/`,{
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then( res => res.json())
		.then( data => {
			setDataSet(data)

		})

	}, [])

	const CategoriesSet = dataSet.map(category => {
		if(category.createdBy === user.id && category.description === undefined){
			return(

					<Col xs={12} md={3}>
						<Card bg="dark" key={category._id} className="mt-3">
						  <Card.Body>
						    <Card.Title>{category.categoryName}</Card.Title>
						    <Card.Subtitle className={category.categoryType}>{category.categoryType}</Card.Subtitle>
						  </Card.Body>
						</Card>
					</Col>

				)
		}
	})

	function createCategory(){
		Router.push("/categories/create-category")
	}

	return(

		<Fragment>
			<Head>
				<title> Categories || Budget Tracker</title>
			</Head>
			<>
				<h1>Categories</h1>
				<Button variant="success" type="button" onClick={() => createCategory()}>Create New Category</Button>
			</>	
			<Row>
				{CategoriesSet}
			</Row>
		</Fragment>

	)

}