import Jumbotron from "react-bootstrap/Jumbotron"
import { Row, Col } from "react-bootstrap"
import Carousel from 'react-bootstrap/Carousel'

const date = new Date()

export default function Banner({data}){

	return(

			<Row>
				<Col xs={12}>
						<Carousel className="mt-3">
						  <Carousel.Item>
						    <a href="https://www.takechargeamerica.org/tips-for-tracking-your-expenses/" target="_blank">
						    <img
						      className="d-block w-100"
						      src="https://images.pexels.com/photos/53621/calculator-calculation-insurance-finance-53621.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
						      alt="Tips to keep your monthly budget on track"
						    />
						    </a>
						    <Carousel.Caption>
						      <h4>Tips to keep your monthly budget on track</h4>
						    </Carousel.Caption>
						  </Carousel.Item>
						  <Carousel.Item>
						  	<a href="https://www.daveramsey.com/blog/the-truth-about-budgeting/" target="_blank">
						    <img
						      className="d-block w-100"
						      src="https://images.pexels.com/photos/4386373/pexels-photo-4386373.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
						      alt="15 Practical Budgeting Tips"
						    />
						    </a>
						    <Carousel.Caption>
						      <h4>15 Practical Budgeting Tips</h4>
						    </Carousel.Caption>
						  </Carousel.Item>
						  <Carousel.Item>
						 	 <a href="https://www.nerdwallet.com/article/finance/tracking-monthly-expenses/" target="_blank">
						    <img
						      className="d-block w-100"
						      src="https://images.pexels.com/photos/4604406/pexels-photo-4604406.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
						      alt="5 Steps for Tracking Your Monthly Expenses"
						    />
						    </a>
						    <Carousel.Caption>
						      <h4>5 Steps for Tracking Your Monthly Expenses</h4>
						    </Carousel.Caption>
						  </Carousel.Item>
						</Carousel>
				</Col>
				<Col xs={12}>
					<Jumbotron>
						<h1>Welcome to {data.title}</h1>
						<p>{data.content}</p>
					</Jumbotron>
				</Col>
			</Row>

		)

}