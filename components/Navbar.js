import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import NavDropdown from "react-bootstrap/NavDropdown"
import { Fragment } from "react"
import { useContext } from "react"
import UserContext from "../UserContext"

import Link from "next/link"

export default function NavBar(){

	const { user } = useContext(UserContext)

	return(
	
		<Navbar bg="dark" variant="dark" expand="lg">
			<Link href="/">
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
					{ user.id === null ?
						<Nav className="ml-auto">
							<Link href="/register">
								<a className="nav-link">Register</a>
							</Link>
							<Link href="/login">
								<a className="nav-link">Login</a>
							</Link>
						</Nav>  :
						<Fragment>
							<Nav className="mr-auto">
								<Link href="/categories">
									<a className="nav-link">Categories</a>
								</Link>
								<Link href="/records">
									<a className="nav-link">Records</a>
								</Link>
								<NavDropdown bg="dark" variant="dark" title="Statistics" id="collapsible-nav-dropdown">
									<NavDropdown.Item>
										<Link href="/monthlyIncome">
											<a className="nav-link">Monthly Income</a>
										</Link>
									</NavDropdown.Item>
									<NavDropdown.Item>
										<Link href="/monthlyExpenses">
											<a className="nav-link">Monthly Expenses</a>
										</Link>
									</NavDropdown.Item>
									<NavDropdown.Divider />
									<NavDropdown.Item>
										<Link href="/trend">
											<a className="nav-link">Trend</a>
										</Link>
									</NavDropdown.Item>
									<NavDropdown.Item>
										<Link href="/breakdown">
											<a className="nav-link">Breakdown</a>
										</Link>
									</NavDropdown.Item>
								</NavDropdown>
							</Nav>
							<Nav className="ml-auto">
									<Link href="/logout">
										<a className="nav-link">Logout</a>
									</Link>
							</Nav>
						</Fragment>
					}
			</Navbar.Collapse>
		</Navbar>

	)

}